import unittest

from src.main import MonitoringInfo

test_JSON = {"points"
             :[{"passingTimes":
                            [{"destination":
                                  {"fr":"BOONDAEL GARE","nl":"BOONDAAL STATION"},
                              "expectedArrivalTime":"2020-09-02T12:15:00+02:00",
                              "lineId":"25"},
                             {"destination":
                                  {"fr":"EUROCONTROL","nl":"EUROCONTROL"},
                              "expectedArrivalTime":"2020-09-02T12:23:00+02:00",
                              "lineId":"62"},
                             {"destination":
                                  {"fr":"BOONDAEL GARE","nl":"BOONDAAL STATION"},
                              "expectedArrivalTime":"2020-09-02T12:24:00+02:00",
                              "lineId":"25"},
                             {"destination":{"fr":"EUROCONTROL","nl":"EUROCONTROL"},
                              "expectedArrivalTime":"2020-09-02T12:38:00+02:00",
                              "lineId":"62"}],
                "pointId":"6204"},
               {"passingTimes":
                            [{"destination":
                                  {"fr":"BORDET STATION","nl":"BORDET STATION"},
                              "expectedArrivalTime":"2020-09-02T12:25:00+02:00",
                              "lineId":"64"},
                             {"destination":
                                  {"fr":"MACHELEN","nl":"MACHELEN"},
                              "expectedArrivalTime":"2020-09-02T12:15:00+02:00",
                              "lineId":"65"},
                             {"destination":
                                  {"fr":"BORDET STATION","nl":"BORDET STATION"},
                              "expectedArrivalTime":"2020-09-02T12:37:00+02:00",
                              "lineId":"64"},
                             {"destination":
                                  {"fr":"MACHELEN","nl":"MACHELEN"},
                              "expectedArrivalTime":"2020-09-02T12:24:00+02:00",
                              "lineId":"65"}],
                "pointId":"2976"}]}

class JsonParseTest(unittest.TestCase):
    def test(self):
        MonitoringInfo(test_JSON)