Small web tool that shows arrival times for MIVB stops. 

An MIVB access token is needed to use this application. You will need a "Consumer key" and "Consumer secret" from MIVB in order to generate one. You can find these [here](http://opendata.stib-mivb.be).

Run as:
```main.py MIVB_Consumer_key MIVB_Consumer_secret```

To see the arrival times of, for example, Petillon (8211) and Hankar (8221), go to *your.address/times/8211/8221*.
Up to ten stops can be added.