#!/usr/bin/env python3
import sys

from flask import (Flask, render_template, Markup)
from datetime import datetime
import requests
import base64

app = Flask(__name__)
language = "nl"


class Vehicle():
    def __init__(self, data):
        self.nr = data["lineId"]
        self.destination = data["destination"][language]
        self.time = datetime.strptime(data["expectedArrivalTime"],
                                      "%Y-%m-%dT%H:%M:%S%z")

    def format_time(self):
        return self.time.strftime("%H:%M")

    def __str__(self):
        return "Line " + str(self.nr) + " to " + str(self.destination) + "\n\t" + str(self.time)

    def to_html(self):
        return "<td>" + str(self.nr) + "</td>" + "<td>" + str(self.destination) + "</td>" + "<td>" + self.format_time() + "</td>"


class Stop():
    def __init__(self, data):
        self.vehicles = list(map(lambda e: Vehicle(e), data["passingTimes"]))
        self.vehicles.sort(key=lambda v: v.nr)
        self.id = data["pointId"]
        self.name_fr = "No french name"
        self.name_nl = "No dutch name"
        self.geo_loc = (0,0)

    def __str__(self):
        res = ""
        for veh in self.vehicles:
            res += str(veh)
        return res

    def to_html(self):
        res = ""
        for veh in self.vehicles:
            res += "<tr>"+veh.to_html()+"</tr>"
        return res

    def set_stop_data(self, name_fr, name_nl, geo_loc):
        self.name_fr = name_fr
        self.name_nl = name_nl
        self.geo_loc = geo_loc

    def get_name(self, langauge):
        return self.name_fr if language == "fr" else self.name_nl

class MonitoringInfo():
    def __init__(self, time_data, info_data):
        self.stops = dict(map(lambda e: (e["pointId"], Stop(e)), time_data["points"]))
        for i in info_data["points"]:
            id = i["id"]
            geo = (i["gpsCoordinates"]["latitude"], i["gpsCoordinates"]["longitude"])
            name_fr = i["name"]["fr"]
            name_nl = i["name"]["nl"]
            self.stops[id].set_stop_data(name_fr, name_nl, geo)



    def __str__(self):
        res = ""
        for stop in self.stops:
            res += str(stop)
        return res

    def to_html(self):
        table_header = """<table>
<tr>
<th>Line</th>
<th>Destination</th>
<th>Time</th>
</tr>
"""
        table_footer = "</table>"
        res = ""
        for stop in self.stops.values():
            res += "<h2>" + stop.get_name(language) + " (" + str(stop.id) + ")</h2>" + table_header + stop.to_html() + table_footer
        return Markup(res)

    def get_title(self):
        nams = ", ".join(map(lambda s: s.get_name(language), self.stops.values()))
        return "Times for " + nams


def get_times_and_info(ids, last_run_error = False):
    time_base_url = "https://opendata-api.stib-mivb.be/OperationMonitoring/4.0/PassingTimeByPoint/"
    info_base_url = "https://opendata-api.stib-mivb.be/NetworkDescription/1.0/PointDetail/"
    global token
    headers = {"Authorization": "Bearer " + token,
               "Accept": "application/json"}
    fixed_ids = ids.replace("/", "%2C")
    time_url = time_base_url + fixed_ids
    info_url = info_base_url + fixed_ids
    print("Requests:\n\t" + time_url + "\n\t" + info_url)
    times = requests.get(time_url, headers=headers)
    infos = requests.get(info_url, headers=headers)
    print("Results:\n\t" + str(times.status_code) + "\n\t" + str(infos.status_code))
    if (times.status_code > 400) or (infos.status_code > 400):
        if last_run_error:
            raise(Exception("Error getting page: status codes " + str(times.status_code) + "," + str(infos.status_code)))
        else:
            token = get_token(consumer_key, consumer_secret)
            get_times_and_info(ids, True)
    else:
        return MonitoringInfo(times.json(), infos.json())


@app.route("/times/<path:ids>")
def with_ids(ids):
    info = get_times_and_info(ids)
    return render_template("times.html", content=info.to_html(), title=info.get_title())


def str_to_base64(string):
    ### https://stackabuse.com/encoding-and-decoding-base64-strings-in-python/
    string_bytes = string.encode('ascii')
    base64_bytes = base64.b64encode(string_bytes)
    return base64_bytes.decode('ascii')


def get_token(key, secret):
    token_url = "https://opendata-api.stib-mivb.be/token"
    post_data = "grant_type=client_credentials"
    base64_key_secret = str_to_base64(key + ":" + secret)
    header = {"Authorization": "Basic " + base64_key_secret}
    res = requests.post(token_url, data=post_data, headers=header)
    return res.json()["access_token"]


if __name__ == "__main__":
    global token, consumer_key, consumer_secret
    consumer_key = sys.argv[1]
    consumer_secret = sys.argv[2]
    token = get_token(consumer_key, consumer_secret)
    app.run(host='0.0.0.0', port=5050)